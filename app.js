const { v4: uuidv4 } = require('uuid');
var AWS = require('aws-sdk');

var handler = async ({ path, pathParameters, httpMethod, body }) => {
  let dynamodb = new AWS.DynamoDB({
    apiVersion: '2012-08-10',
    endpoint: 'http://dynamodb:8000',
    region: 'us-west-2',
    credentials: {
      accessKeyId: '2345',
      secretAccessKey: '2345',
    },
  });

  let docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: '2012-08-10',
    service: dynamodb,
  });

  if (httpMethod == 'POST') {
    const createParams = {
      TableName: 'Envio',
      Item: {
        id: uuidv4(),
        pendiente: String(new Date()),
        ...JSON.parse(body),
        fechaAlta: String(new Date()),
      },
    };
    try {
      const body = JSON.stringify(createParams.Item);
      await docClient.put(createParams).promise();
      return {
        statusCode: 200,
        headers: { 'content-type': 'application/json' },
        body: body,
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        headers: { 'content-type': 'text/plain' },
        body: `${error}`,
      };
    }
  }
  if (httpMethod == 'PUT') {
    const idEnvio = (pathParameters || {}).id_envio || false;
    if (id_envio) {
      const updateParams = {
        TableName: 'Envio',
        Key: {
          id: idEnvio,
        },
        UpdateExpression: 'remove pendiente',
        ConditionExpression: 'attribute_exists(pendiente)',
        RetunValues: 'ALL_NEW',
      };
      try {
        await docClient.update(updateParams).promise();
        return {
          statusCode: 200,
          headers: { 'content-type': 'text/plain' },
          body: `${idEnvio}`,
        };
      } catch (error) {
        console.error(error);
        return {
          statusCode: 500,
          headers: { 'content-type': 'text/plain' },
          body: `${error}`,
        };
      }
    } else {
      return {
        statusCode: 400,
        body: 'id invalido',
      };
    }
  }
  if (httpMethod == 'GET') {
    const params = {
      TableName: 'Envio',
      IndexName: 'EnviosPendientesIndex',
    };
    try {
      const envios = await docClient.scan(params).promise();
      return {
        statusCode: 200,
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(envios),
      };
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        headers: { 'content-type': 'text/plain' },
        body: `${error}`,
      };
    }
  }
};

exports.handler = handler;
