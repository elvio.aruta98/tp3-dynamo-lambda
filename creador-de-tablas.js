var AWS = require('aws-sdk');

AWS.config.update({
  region: 'us-west-2',
  endpoint: 'http://localhost:8000',
});

AWS.config.loadFromPath('./config.json');
var dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

var params = {
  TableName: 'Envio',
  KeySchema: [{ AttributeName: 'id', KeyType: 'HASH' }],
  AttributeDefinitions: [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'pendiente', AttributeType: 'S' },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1,
  },
  GlobalSecondaryIndexes: [
    {
      IndexName: 'EnviosPendientesIndex',
      KeySchema: [
        { AttributeName: 'id', KeyType: 'HASH' },
        { AttributeName: 'pendiente', KeyType: 'RANGE' },
      ],
      Projection: {
        ProjectionType: 'ALL',
      },
      ProvisionedThroughput: {
        ReadCapacityUnits: 1,
        WriteCapacityUnits: 1,
      },
    },
  ],
};
dynamodb.createTable(params, function (err, data) {
  if (err) {
    console.error('Error creando la tabla');
  } else {
    console.log('Tabla creada con exito');
  }
});
