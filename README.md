## TP3-Dynamo-Lambda
Trabajo practico 3 - Node, AWS Lambda, DynamoDB

- **Legajo:** 43988
- **Nombre:** Elvio Aruta

### Como correr el proyecto

**Requerido:**

- Docker, Node, SAM CLI

**Comandos:**

**Docker:**

```
    docker network create awslocal
    docker run -p 8000:8000 --network awslocal --name dynamodb amazon/dynamodb-local:1.15.0 -jar DynamoDBLocal.jar -sharedDb
```

**Proyecto:**

```
    git clone https://gitlab.com/elvio.aruta98/tp3-dynamo-lambda.git
    cd tp3-dynamo-lambda/
    npm i
    node creador-de-tablas.js
    sam local start-api --docker-network awslocal
```
### Endpoints

Los Endpoints estan documentados en el template.yaml
